package com.system;

import java.util.ArrayList;

public class Doctor {
    private String name;
    private ArrayList<Patient> patientList;
    private Hospital hospital;
    private int doctorID;


    public Doctor(){
        setName("null");
        setPatientList(new ArrayList<Patient>());
        setDoctorID(-1);
    }

    public ArrayList<Patient> getPatientList() {
        return patientList;
    }

    public void setPatientList(ArrayList<Patient> patientList) {
        this.patientList = patientList;
    }

    public int getDoctorID() {
        return doctorID;
    }

    public void setDoctorID(int doctorID) {
        this.doctorID = doctorID;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Hospital getHospital() {
        return hospital;
    }

    public void setHospital(Hospital hospital) {
        this.hospital = hospital;
        hospital.addDoctorToList(this);
    }
}
