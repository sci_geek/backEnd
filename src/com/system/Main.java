package com.system;

import java.time.LocalDate;

public class Main {

    public static void main(String[] args) {
        Hospital h1 = new Hospital();
        h1.setAdminID(1000);
        h1.setName("hospital 1");

        Doctor d1 = new Doctor();
        d1.setDoctorID(1);
        d1.setName("dave");
        d1.setHospital(h1);


        Doctor d2 = new Doctor();
        d2.setDoctorID(2);
        d2.setName("steve");
        d2.setHospital(h1);

        Patient p1 = new Patient();
        p1.setDoctor(d1);
        p1.setDoctorNotes("smells like a foot");
        p1.setHeight(2.3);
        p1.setHeight(103);
        p1.setDateOfBirth(12, 1, 1980);

        System.out.println(p1.getAge());

        p1.setDateOfBirth(14, 1, 1980);

        System.out.println(p1.getAge());

        p1.setEthnicity(Ethnicity.Black);

        System.out.println(p1.getEthnicity());


    }
}
