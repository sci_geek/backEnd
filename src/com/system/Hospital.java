package com.system;

import java.util.ArrayList;

public class Hospital {
    private String name;
    private ArrayList<Patient> patientList;
    private ArrayList<Doctor> doctorList;
    private int AdminID;

    public Hospital() {
        name = "null";
        setPatientList(new ArrayList<Patient>());
        setDoctorList(new ArrayList<Doctor>());
        setAdminID(-1);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Patient> getPatientList() {
        return patientList;
    }

    public void setPatientList(ArrayList<Patient> patientList) {
        this.patientList = patientList;
    }

    public void addPatientToList(Patient p){
        getPatientList().add(p);
        //p.setHospital(this);
        if (p.getHospital()!=this){
            p.setHospital(this);
        }
    }

    public ArrayList<Doctor> getDoctorList() {
        return doctorList;
    }

    public void setDoctorList(ArrayList<Doctor> doctorList) {
        this.doctorList = doctorList;
    }

    public void addDoctorToList(Doctor d){
        getDoctorList().add(d);
        if (d.getHospital()!=this){
            d.setHospital(this);
        }
    }

    public int getAdminID() {
        return AdminID;
    }

    public void setAdminID(int adminID) {
        AdminID = adminID;
    }







}
