package com.system;

import java.time.LocalDate;
import java.time.Period;


enum Sex {
    Male, Female, Other
}

enum Ethnicity {
    Black, White, Yellow
}

public class Patient {private String name;
    private String userName;
    private String email;
    private LocalDate dateOfBirth;
    private String doctorNotes;
    private double weight;
    private double height;
    private Ethnicity ethnicity;
    private Sex sex;
    private Doctor doctor;
    private Hospital hospital;

    public Patient() {
        setUserName("null");
        setEmail("null");
        setDateOfBirth(null);
        setDoctorNotes("");
        setWeight(-1);
        setHeight(-1);
        setEthnicity(null);
        setSex(null);
        setDoctor(new Doctor());
        setHospital(new Hospital());

    }

    public int getAge() {
        return Period.between(dateOfBirth, LocalDate.now()).getYears();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public void setDateOfBirth(int Day, int Month, int Year) {
        this.setDateOfBirth(LocalDate.of(Year,Month,Day));
    }

    public String getDoctorNotes() {
        return doctorNotes;
    }

    public void setDoctorNotes(String doctorNotes) {
        this.doctorNotes = doctorNotes;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public Ethnicity getEthnicity() {
        return ethnicity;
    }

    public void setEthnicity(Ethnicity ethnicity) {
        this.ethnicity = ethnicity;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Hospital getHospital() {
        return hospital;
    }

    public void setHospital(Hospital hospital) {
        this.hospital = hospital;
        hospital.addPatientToList(this);
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }
}
